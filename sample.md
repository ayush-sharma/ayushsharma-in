---
layout: post
title:  "Sample post, for checking fonts and stuff"
number: 0
date:   2020-03-29 00:00
tags: automation containers
---
## Launch your Pipelines container

With the clone and checkout complete, go to your clone directory and launch the same Docker container as specified in your Pipelines.

To begin, `cat` your `bitbucket-pipelines.yml` to identify the container you're using.

```yaml
image: node:11.13.0-alpine

pipelines:
  branches:
    master:
      - step:
          caches:
            - node
          script:
            - apk add python3
            - npm install -g serverless
            - serverless config credentials --stage dev --provider aws --key ${key} --secret ${secret}
            - serverless deploy --stage dev
```

Launch the `node:11.13.0-alpine` container and mount your current directory:

```bash
docker run -v `pwd`:/mycode -it node:11.13.0-alpine /bin/sh
```

Once the command runs successfully, you'll be in the Docker shell. Change to the mounted directory to ensure the code is present.

```bash
cd /mycode
```

Nice! Our Serverless package built successfully. As you can see, our CloudFormation template is right there.

What we've done so far might now seem like much immediately, but we have quite a lot of diagnostic information already:


1. If there were any errors during the package step, then you've already eliminated Pipelines and everything that happens after it as a potential culprit. Narrowing down on problem areas is critical to diagnosing issues.
2. We can build the previous version of our project and compare the sizes of the CloudFormation templates, which can be diagnostically significant. This process is a lot harder to do on Bitbucket, especially if your build is continuously failing.
3. Replicating the environment locally also allows us to play with container versions. If you've been changing container versions and deploying Pipelines to test them, this will be a huge time-saver.
4. You can now also check the memory and CPU consumption of your build container in a controlled environment. If your Pipelines have been complaining of resource constraints, you now have a way to test it.

Local debugging avoids the friction of triggering your Pipeline every time you make a change.

Which means easier debugging, better debugging, and overall happier engineers :)