---
title: Video Games
layout: page
desc: "༼つಠ益ಠ༽つ ─=≡ΣO))"
permalink: /video-games
---
{% assign posts = site.posts| where:"Category", "Video Games" | sort: 'date' | reverse %}
{% for post in posts %}
{% include article-link.html post=post %}
{% endfor %}