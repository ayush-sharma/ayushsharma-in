---
title: Technology
layout: page
desc: "(⊙＿⊙')"
permalink: /technology
---
{% assign posts = site.posts| where:"Category", "Technology" | sort: 'date' | reverse %}
{% for post in posts %}
{% include article-link.html post=post %}
{% endfor %}