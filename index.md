---
layout: page
permalink: /
---
<small>I'm Ayush. I will hug you (づ｡◕‿‿◕｡)づ. Do not resist.</small>
<p>
    <h2>The latest</h2>
    {% assign posts = site.posts | sort: 'date' | reverse %}
    {% for post in posts limit: 5 %}
    {% include article-link.html post=post %}
    {% endfor %}
</p>

<p>
    <h2>Favourites</h2>
    {% assign posts = site.posts | where:"is_pinned", true | sort: 'date' | reverse %}
    {% for post in posts limit: 5 %}
    {% include article-link.html post=post %}
    {% endfor %}
</p>