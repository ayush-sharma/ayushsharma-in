#/bin/bash

echo "Checking post counts..."
cd _posts
a=`head -6 *.md | grep "number: " | awk '{print $2}' | sort | uniq -c | awk '{print $2}' | wc -l`
b=`ls -1 *.md | wc -l`

echo "Post number count: "$a
echo "Post total count: "$b

if [ "$a" -eq "$b" ]; then
  echo "They're equal"
  exit 0
else
    echo "Post number count mismatch!"
    exit 1
fi
cd ../

echo "Resizing and optimizing images..."
cd static/images
mkdir -p img-thumbs img-normal
cp content/*.gif img-thumbs/; cp content/*.gif img-normal/
cp content/*.svg img-thumbs/; cp content/*.svg img-normal/
cp content/*.jpg img-thumbs/; cp content/*.jpg img-normal/
cp content/*.png img-thumbs/; cp content/*.png img-normal/

cd img-thumbs
mogrify -resize 422x316 *.png
mogrify -format jpg -resize 422x316 *.jpg
for i in *.png; do optipng -o5 -quiet "$i"; done
jpegoptim -sq *.jpg

cd ..
cd img-normal
mogrify -resize 1024x768 *.png
mogrify -format jpg -resize 1024x768 *.jpg
for i in *.png; do optipng -o5 -quiet "$i"; done
jpegoptim -sq *.jpg