# Personal notes of Ayush Sharma

[![Netlify Status](https://api.netlify.com/api/v1/badges/03538759-f6f0-408f-a613-d81c9bae1f4a/deploy-status)](https://app.netlify.com/sites/notesayushsharma/deploys)

# Jekyll installation / testing

```bash
docker run --network host --rm \
  --volume="$PWD:/srv/jekyll:Z" \
  -it jekyll/jekyll:4.2.2 \
  gem install --user-install bundler jekyll; bundle install; bundle exec jekyll serve --incremental --config _config.yml,_config-local.yml
```

# Image compression

```bash
brew install optipng jpegoptim imagemagick pngquant
cd static/images
mkdir -p img-thumbs img-normal
cp content/*.gif img-thumbs/; cp content/*.gif img-normal/
cp content/*.svg img-thumbs/; cp content/*.svg img-normal/
cp content/*.jpg img-thumbs/; cp content/*.jpg img-normal/
cp content/*.png img-thumbs/; cp content/*.png img-normal/

cd img-thumbs
mogrify -resize 422x316 *.png
mogrify -format jpg -resize 422x316 *.jpg
pngquant 64 *.png --force --ext .png
jpegoptim -sq *.jpg

cd ..
cd img-normal
mogrify -resize 1024x768 *.png
mogrify -format jpg -resize 1024x768 *.jpg
pngquant 64 *.png --force --ext .png
jpegoptim -sq *.jpg
```

# Post Metadata

List of possible values for `post` front matter.

```yaml
layout: post
title:  ""
number: <integer>
date:   <2005-06-30 00:00>
excerpt: ""
Category: <Life Stuff/Technology/Video Games>
banner_img: "*.<jpg/png/svg>"
is_pinned: true # If post is pinned to home page.
is_series: true # If post is part of series
series_title: "" # Series title; MUST be same across posts in the same series.
```