---
layout: post
title: "Nemo - The Ubuntu file manager you didn’t know you needed"
number: 120
date: 2022-01-25 01:00
excerpt: "Celebrate 2022 with a shiny new file manager for Ubuntu!"
Category: Technology
banner_img: "2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-home.jpg"
---
So... I finally got bored with Nautilus.

Ubuntu has been my daily driver for the last several years. I remember when Linux used to be a second-class citizen in the desktop space, only used by nerds and devs who could unravel its arcane and obscure mysteries. But it has come a long way. Not only is it a lot more user-friendly with a plethora of customisation options, but Steam, and especially Proton, has added first-class gaming support for Linux. I haven’t had a dual boot Ubuntu/Windows system in quite a while and that’s a huge win.

Nautilus seems out of place in this brave new world, like it's still stuck in the old way of doing things. It’s become more stable, sure, but it doesn't match up to Windows’s Explorer or Mac’s Finder. So a few days ago I just searched for “[best ubuntu file manager 2022](https://duckduckgo.com/?q=best+ubuntu+file+manager+2022)”... and that’s how I found Nemo. 

Nemo ([wiki](https://en.wikipedia.org/wiki/Nemo_(file_manager)), [GitHub](https://github.com/linuxmint/nemo)) is the file manager for the Cinnamon desktop environment. It’s beautiful, snappy, and useful. It’s beautiful, snappy, and, erm, useful. Compared to Nautilus, browsing remote networks is faster and the feature-set more comprehensive.

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-comparison.jpg" width="1024" alt="Image showing Nautilus file manager on the left and Nemo file manager on the right. Using Ubuntu 20.04 LTS." %}

## Feature list
These are some of the features of Nemo that I found most useful:

1. Type-ahead search.
2. [Menu bar](https://askubuntu.com/questions/1095891/nautilus-menu-bar-missing-in-ubuntu-18-04)! Plus customizable toolbar, customizable tooltip, and customizable date format.
3. You can open a separate panel to browse files, besides new tabs and windows.
4. The sidebar has a tree view! Also shows filesystem free space graphically.
5. Copying, moving, or extracting files shows progress in a separate mini-window (like Mac). A queue manages all file operations.
6. Advanced bookmarks editor - like the one in Firefox.
7. Separate view settings for separate folders.
8. The status bar shows number of files and available disk space.
9. File renames are in-place (like Mac) rather than in a popup.
10. Trash customisation - bypass Trash on delete or add a separate button to bypass Trash.
11. Built-in bulk-rename (though I haven’t used this one yet).
12. [Extensions](https://github.com/linuxmint/nemo-extensions)!

## Installing Nemo

Installing Nemo is about as easy as you would expect. Do this:

```shell
sudo apt install nemo
```

And voila! You should now have another “Files” app in your Applications Menu. Keep Nautilus around till you’re comfortable with Nemo, and then you can nuke Nautilus for good.

## Set Nemo as the default file manager

To set Nemo as the default file manager, do this:

```shell
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search
gsettings set org.gnome.desktop.background show-desktop-icons false
gsettings set org.nemo.desktop show-desktop-icons true
```

## Screenshots

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-view-menu.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with View menu open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-bookmarks-menu.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Bookmarks menu open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-views-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with View preferences open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-behavior-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Behavior preferences open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-display-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Display preferences open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-toolbar-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Toolbar preferences open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-context-menu-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Context Menu preferences open." %}

{% include article-image.html src="2022-01-25-nemo-file-manager-ubuntu-20.04-linux-nautilus-alternative-plugins-preferences.jpg" width="1024" alt="Image showing Nemo file manager on Ubuntu 20.04 LTS with Plugins preferences open." %}

Enjoy your new Ubuntu :)