---
layout: post
title:  "My 10 favorite books so far! - 2021 edition"
number: 104
date:   2021-07-10 00:00
excerpt: "These are the ten books that I found most insightful, thought-provoking, and life-changing."
Category: Life Stuff
banner_img: "2021-07-10-my-10-favorite-books-so-far.png"
is_pinned: true
---
Hemingway once said that writing is easy, you just sit in front of a typewriter... and bleed. I understand that now. Simply selecting ten books already written by other people was a daunting task.

## 1. The Hitchhiker's Guide to the Galaxy (1978) by Douglas Adams

In 1971, a young British man was lying drunk in a field in Innsbruck, Austria. As he looked up at the stars, feeling around for his copy of The Hitchhiker's Guide to Europe, he wondered why no one ever wrote a hitchhiker's guide to the galaxy. This idea searched his mind for something to connect with, failed, and Douglas Adams promptly forgot about the whole thing for several years. And that's how the story of The Hitchhiker’s Guide began.

The H2G2 series, a trilogy of five books, is the story of the life of Arthur Dent. It begins with the end. Arthur Dent, the protagonist, finds bulldozers outside his house, ready to demolish it to make way for a highway bypass. After several hours of protests, threats, and tomfoolery with the man in charge, who happened to be a direct male-line descendant of Genghis Khan, Arthur is unable to prevent this fate. But he does not get much time to feel disheartened, because soon after that, the planet Earth is demolished by an alien fleet to make way for an inter-galactic hyperspace bypass. Arthur is rescued in time by his best friend, Ford Prefect, who was an alien pretending to be an out of work actor at the time.

The science fiction genre is usually home to writers who want to bring the strange and the absurd just a little closer to reality, those who dare to explore what our minds can produce and go beyond so we can understand our stories better. What need do we have in looking up at the stars? Do we seek to find something familiar, something in our own image, in the coldness of space? Could our human, mortal stories exist somewhere out there as well? Douglas Adams' work has become legend not only because of his imagination, but because he married everyday life, science fiction, and comedy in a way that few before him or since have accomplished. With Arthur Dent, the last survivor of the human race, and his journey through an alien universe, we find bewildering things, like aliens who build planets, a paranoid android, and the ruler of the entire Universe, but we also find the familiar, like love, loneliness, the lust for power, and a penchant for fancy restaurants (in this case, a restaurant at the end of the Universe).

Douglas Adams combined his significant writing ability with a razor-sharp wit. If you enjoy reading sentences like “The ships hung in the sky in much the same way that bricks don't” or “Anyone who is capable of getting themselves made President should on no account be allowed to do the job” or “My capacity for happiness you could fit into a matchbox without taking out the matches first”, you will enjoy reading his work. He wrote 13 novels in the short time he was with us, and each is a gem that can take you on a different journey through his universe, for those know where their towel is.

This is also the best book ever written by man. You should read it. You can ignore the rest of the list if you want, it's fine, I won't mind.

## 2. Sapiens: A Brief History of Humankind (2014) by Yuval Noah Harari

What is history? Is it only a recounting of past events as they happened, or does it require further scrutiny to understand why they happened in the first place? Why is human history a history of wars, conquests, religions, and money? Why are we the apex predator, capable of supplanting other species that are capable of destroying us as individuals? Why did we create writing and the sciences and not the tigers and the Neanderthals? Why are we, Homo Sapiens, the superior animal?

Yuval Noah Harari, an Israeli historian, attempts to answer these questions in his thought-provoking Sapiens: A Brief History of Humankind. His thesis combines developments across evolutionary biology, culture, and philosophy, and provides an answer to why we are the way we are. He argues that the reason for our uniqueness as a species, and indeed the uniqueness of our achievements, is because of our singular ability to believe in fictions: the power to imagine and believe in things that do not exist.

In his book, he traverses our history from 70,000 years ago, beginning at the cognitive revolution, to the modern-day man, and argues that our society is based on ideas that work simply because we believe that they do. Why do we exchange goods and services for money? Because we imbue it with that belief of exchange. Why do we believe in our nation states? Because we imbue it with the belief of belongingness. Why is there religion? Because we believe there is. This power of our species to come together and believe in abstract concepts on a mass scale is what separates us from the other animals, and indeed what makes us the superior animal.

He follows up Sapiens with 2 other books: Homo Deus, which a brief prediction of tomorrow, and 21 Lessons for the 21st Century, a list of dos and don’ts for the average reader based on the lessons of our past and the predictions for our future. All 3 of his books are very approachable and are put together for the lay reader, which makes contemplating his ideas and reflecting on them as we go about our daily lives that much more subtle and engaging.

## 3. The Doomsday Machine: Confessions of a Nuclear War Planner (2017) by Daniel Ellsberg

Daniel Ellsberg, an economist, political activist, and former US military analyst, became famous in 1971 when he released the Pentagon Papers: 7,000 pages of confidential US Defense Department documents which exposed that four US Presidential administrations lied to the public about the real reasons and casualties of the Vietnam War. This caused major outrage among the public and solidified public support against the Vietnam War, eventually leading to the passing of the War Powers Act in the US, which limited the US President's authority to go to war.

Widely regarded as one of the foremost whistleblowers of the 21st century, he follows up The Pentagon Papers with The Doomsday Machine, a sobering and terrifying look at the state of nuclear war planning today, which he directed very closely as part of his work with the RAND Corporation, a US Defense Department contractor. He states that along with Climate Change, the proliferation of nuclear weapons is perhaps the most catastrophic existential crisis facing us today. His terrifying argument is as follows: that in order for nuclear deterrence to work, the responsibility must be broadly delegated to a wide range of unknown individuals who can carry out the attack. Second, that these individuals must be willing and able to end all life on Earth for the enemy to feel deterred in the first place. These strategies, according to Ellsberg, ensure that nuclear winter is not a matter of if, but when.

Before he released The Pentagon Papers, Ellsberg's work was primarily concerned with nuclear war planning for the United States. Overcome with guilt at the doomsday machine he was helping create, he released this book as a tell-all and a warning to the world, in the hopes it would inspire action and the eventual dismantling of the greatest machinery of mass-suicide ever created. This revelation has required humanity as a whole to learn a new, much darker word: omnicide, the mass-suicide of all life on Earth by the use of nuclear weapons.

## 4. Nineteen Eighty-Four (1949) by George Orwell & Brave New World (1932) by Aldous Huxley

The novel Nineteen Eighty-Four, published in 1949, is written in an imagined future, where government surveillance and propaganda are rampant, the thoughts and actions of the general public are subtlety monitored and disproportionately punished, and where the rank-and-file workers are taught the language of fear and submission. The novel popularized the term "Orwellian" to mean any government or institution that speaks the language of tyranny, and teaches its subjects to fear it rather than participate in it. The novel uses irony to contrast what a state ought to do with what a state can become if left unchecked: The Ministry of Love oversees torture, The Ministry of Plenty oversees shortage and rationing, and the Ministry of Truth is responsible for propaganda. Originally written as a cautionary tale, the book's impact on popular culture cannot be overstated, and is frequently referred to when speaking of totalitarian regimes across the globe.

The novel Brave New World was written 17 years earlier. While it explores the same themes and cautionary tales as Nineteen Eighty-Four, it is more of a utopic dystopia rather than an outright dystopia. The rulers in Brave New World control the population through the use of drugs, eugenics, and mass entertainment designed to obfuscate and distract, which is perhaps more relevant in modern society, fueled more by the haze of social media rather than the truth of objective reality.

While both novels explore similar themes in very different ways, they have both been lauded for their prescience. The dystopia of Nineteen Eighty-Four can still be seen in several despotic regimes today, and the fake news, mass infotainment, brain-deadening culture of Brave New World is all too apparent for anyone who has been on social media for more than five seconds.

## 5. The Great Derangement: Climate Change and the Unthinkable (2016) by Amitav Ghosh

How do we come together to face a crisis that is larger than our lifetimes? How do we, as writers and citizens, collectively grasp with storms and tornadoes that were seeded by our actions hundreds of years ago? In this non-fiction novel, Amitav Ghosh makes a resounding call-to-action to historians, writers, and politicians, to grasp with the reality that our minds are simply not programmed to come to terms with this present predicament.

In a world where much of our writing is influenced by deeply personal and subjective feelings, and where our politics is less about collective action outside of ourselves and more about whose feelings and traditions are honored the most loudly, Amitav Ghosh's novel comes as a sobering reminder that some things are simply larger than our thoughts and feelings, because since we are a part of nature and not apart from it, our personal struggles and upheavals can simply never exceed what nature can throw at us.

Fiction, he argues, is the only realm capable of creating a reality that can allow our minds to grasp this new existential threat, and he urges us writers to think outside ourselves, and bridge the gap between what we are imagining today and what nature is creating around us every single day, for better or for worse.

## 6. Obedience to Authority (1974) by Stanley Milgram

Every day of every year, we submit ourselves to some authority figure or another. Our teachers, our parents, our bosses. But why do we do it, and to what extent will we obey? What is authority? Is it a submission of free will that we grant to an authority figure, or is it something the authority figure commands? The distinction is critical: because in that fine line lies the responsibility of the action, either with us, or with someone else.

In July 1961, three months after the trial of the Nazi war criminal Adolf Eichmann in Jerusalem, the social psychologist Stanley Milgram placed an advertisement in the local newspaper asking for volunteers, offering them four dollars for an hour of their time. His intention? He wanted to answer a simple question: could Eichmann and millions like him who perpetrated the holocaust simply have been following orders? Were they complicit, or were they innocent? In that fine line lay the difference between good and evil, and the human story of the greatest evil committed on human beings.

The Milgram experiment was simple: volunteers would come in and they would be asked by the experimenter (the Authority figure) to help a participant learn a series of words. The volunteers would repeat a series of words, and if the learner guessed them correctly, then nothing would happen. If the learner made a mistake, the volunteer was to give him an electric shock, up to a level that would kill a man. The volunteer was not told that the learner was an actor, and that the electric shocks were fake. The volunteer was made to believe that they were shocking a man to the death, if asked to do so by the experimenter.

Before the experiment, Milgram polled several students and professors about what the outcome might be, and they concluded that no more than 3 percent of the participants would be willing to shock participants beyond a certain threshold. The results of the Milgram experiment concluded that over 65 percent of ordinary people would be willing to shock a person to death if they felt the commands were coming from a trusted authority figure.

While the Milgram experiment has been questioned for its ethics and its execution, and the applicability of its results to the Nazis is still controversial, the experiment has been repeated several times, across the globe, with several variations, with similar results. While there are variations of the experiment where the subjects perform better than average, the experiment still has chilling consequences, and demonstrates the possibility that ordinary people, like you and me, are capable of committing horrible acts if we feel those actions are required by someone we trust.

## 7. Norse Mythology (2017) by Neil Gaiman

Neil Gaiman is a prize-winning fantasy writer, famous for writing American Gods (now a TV show), Coraline (already a movie by Tim Burton), The Graveyard Book, and Neverwhere (also a TV series) to name a few. His latest book is a re-telling of the famous Norse myths, stories of the wise Norse god Odin, who gave his eye to the Tree of Knowledge in exchange for Wisdom; of Thor, the brash and arrogant god of thunder, who is perhaps a little too attached to his magic hammer; and Loki, the mischievous and deceitful god of, well, mischief and deceit. Fans of the recent Marvel movies will already be familiar with these names, and Gaiman's Norse Mythology can provide interesting background to how these gods came to be, where they came from, and how their godly decisions sometimes mask all too human failings. The book is a story of gods, but is also a story of love, greed, treachery, and a god who thinks every problem can be solved with a magic hammer, and he's not always wrong.

## 8. When Breath Becomes Air (2016) by Paul Kalanithi

Paul Kalanithi, in his last year of neurosurgery at Stanford, began to experience changes in his health and personal life. After a routine checkup with his physician, he was diagnosed with lung cancer. What follows is a transition of a neurosurgeon, acutely aware of the functioning of his own brain, from a doctor to a patient, grappling with the reality of his end. Through his scientific training, his experience as a son and husband, and a student of religion and philosophy, he tries to grapple with his situation, wondering if any system of thought is capable of grasping the entirety of a complete and full human life. What constitutes a good life, and what does it mean when it ends? Is a good life the sum of our experiences, or the sum our experiences with others, and the sum of their experiences, ad infinitum? Can the wholeness of a life really be described in words?

When Breath Becomes Air was published posthumously in 2016 after his courageous battle with cancer. He is survived by his daughter and his wife, who wrote the foreword for the book and provided more insight into his life.

## 9. Between the World and Me by Ta-Nehisi Coates

Between the World and Me is a book-length letter written by the author to his fifteen-year-old son, Samori. After they both watched the acquittal of a police officer after shooting an unarmed African American teenager on the evening news, Samori rushed to his room and locked the door. The book was written as a consolation to his son.

Ta-Nehisi Coates describes in vivid detail the struggles of African Americans in modern America, recounting the history of the movement and his own struggles in childhood. While other books have covered this period of history in greater detail, what is different about this book is that it is not just an autobiographical account of the history, it is letter from a scared father to his son, who is trying to teach him how strong, and how fearless he would have to be to grow up in their land of the free, home of the brave.

While being very specific to the writer's own racial history, it is also a story of persecution in general, about how prejudice and inequality is not about race or class or creed, but about ascribing bone-deep, immovable, insurmountable prejudices to those who we fear are a threat to our own culture or way of being. It is a very moving tale about the writer's struggles, and prepares the mind for an intellectual position that is required in today's politically charged and sometimes hateful climate.

It is important to remember that hatred is not something inherent in us, it is something we are taught when we are unaware of it, and this book helps us strengthen our guard against ideas that seem seductive, but would otherwise poison our thoughts.

## 10. Meditations by Marcus Aurelius

Life can get very depressing, especially when you're reading about cancer, slavery, Nazis, world-ending climate catastrophes, and world-ending nuclear bombs. It is sometimes important, if only as a palate cleanser, to gain some perspective, and what better perspective than from a two-thousand-year-old Roman Emperor, who was surrounded on all sides by wars, conniving politicians, murderous relatives, and the occasional military coup.

Meditations is Marcus Aurelius’ personal diary, where he recorded his thoughts and ideas on Stoic philosophy, which has become more popular thanks to movements such as Minimalism and books such as "The Subtle Art of Not Giving A F*ck", which are based on his philosophy.

He does not write as an Emperor, and does not favor a particular style over the other. He never intended for his notes to be published, and so the writing is simple, straightforward, and intended for no one except himself. And aren't most of our thoughts made that way? He writes about the importance of assessing one's own judgements.

He wrote, "You have the power to strip away many superfluous troubles located wholly in your judgment, and to possess a large room for yourself embracing in thought the whole cosmos, to consider everlasting time, to think of the rapid change in the parts of each thing, of how short it is from birth until dissolution, and how the void before birth and that after dissolution are equally infinite". He writes about the importance of letting go of things outside your control, removing judgement of yourself and others, and how to appreciate the present over what might be.

And so...

"Put an end once for all to this discussion of what a good man should be, and be one." - Marcus Aurelius