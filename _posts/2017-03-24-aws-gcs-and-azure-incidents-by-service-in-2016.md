---
layout: post
title:  "AWS, GCS, Azure and Digital Ocean incidents by service in 2016"
number: 35
date:   2017-03-24 00:00
excerpt: "I pulled the data for technology incidents for the major cloud services. The findings were pretty interesting."
Category: Technology
---
A few days ago I posted on [AWS, Google Cloud, Azure and Digital Ocean incidents in 2016]({% post_url 2017-03-16-aws-gcs-and-azure-incidents-in-2016 %}). Those were incidents by vendor, this post will cover incidents by service for those vendors. The 5 services that faced the most outages are as follows:

{% include article-image.html src="total-aws-gcs-azure-and-digital-ocean-incidents-by-service-2016.jpg" width="800" height="388" alt="Total Amazon Web Services, Google Cloud Platform, Microsoft Azure, and Digital Ocean service incidents in 2016." %}

You can view the data sheet [here](https://docs.google.com/spreadsheets/d/1Q47ci1dtfYOxndAYq4CVRMNJtPqhHgEkqv53ViW86-c/edit?usp=sharing).