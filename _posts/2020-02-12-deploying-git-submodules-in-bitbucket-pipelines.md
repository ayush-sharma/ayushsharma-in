---
layout: post
title:  "Deploying git submodules in Bitbucket Pipelines"
number: 86
date:   2020-02-10 00:00
excerpt: "Git submodules makes it easy have repos for common dependencies, but how to actually clone them in your Bitbucket Pipeline?"
Category: Technology
is_series: true
series_title: "Bitbucket Pipelines"
---
Bitbucket Pipelines is a great deployment tool tightly integrated into Bitbucket. It allows you to trigger deployments directly from repository branches and tags, and you can customize your deployment steps as needed.

One missing feature in Pipelines is that it is not able to automatically clone submodules that are added to your repository as this requires some additional settings. Until this feature gets added by default, there is a simple way to achieve this.

First, we need to make sure our repository has access to clone the sub-modules repository. You can configure this by reading [Cloning another Bitbucket repository in Bitbucket Pipelines]({% post_url 2020-02-10-cloning-another-bitbucket-repo-in-bitbucket-pipelines %}).

Next, the `git submodule update --init --recursive` command can initialize, fetch and checkout any nested submodules. Add it to the beginning of the `script` block.

```yaml
pipelines:
  default:
    - step:
        script:
          - git submodule update --init --recursive
```

The above Pipeline checks out all nested submodules during its execution. We can then use them in subsequent build steps.

{% include article-image.html src="2020-02-12-deploying-git-submodules-in-bitbucket-pipelines.png" alt="Deploying git submodules in Bitbucket Pipelines" %}

## Resources
- [Git Tools - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)