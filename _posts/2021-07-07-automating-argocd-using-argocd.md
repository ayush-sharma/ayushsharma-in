---
layout: post
title:  "Automating ArgoCD using ArgoCD!"
number: 95
date:   2021-07-07 00:00
excerpt: "In this tutorial, I will show you how to automatically create multiple Applications in ArgoCD using ArgoCD!"
Category: Technology
banner_img: "automating-argocd-with-argocd-deployment.png"
is_series: true
series_title: "ArgoCD"
---
We have already seen how [ArgoCD makes pull-based GitOps deployments simple]({% post_url 2021-07-06-getting-started-with-argocd %}). In this tutorial, I'll show you how to automatically create multiple Applications in ArgoCD using ArgoCD!

Since ArgoCD's job is to listen to a repo and apply the Manifest files it finds to the cluster, we can use this approach to configure ArgoCD internals as well. In our last example, we used the GUI to create a sample Nginx application with three replicas. We will use the same approach as before. But this time, we will create an Application from the GUI, which will deploy three separate applications - `app-1`, `app-2`, and `app-3`.

## Configuring our child applications

Let's start by creating the Manifest files for our three applications. In my [`example-assets` repository](https://gitlab.com/ayush-sharma/example-assets), I have [created three applications under `argocd/my-apps`](https://gitlab.com/ayush-sharma/example-assets/-/tree/main/argocd/my-apps). All three applications are Nginx with three replicas. Remember to create each application in its own folder.

This is `my-apps/app-1/app.yml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-app-1
  labels:
    app: nginx-app-1
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-app-1
  template:
    metadata:
      labels:
        app: nginx-app-1
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```

This is `my-apps/app-2/app.yml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-app-2
  labels:
    app: nginx-app-2
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-app-2
  template:
    metadata:
      labels:
        app: nginx-app-2
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```

And this is `my-apps/app-3/app.yml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-app-3
  labels:
    app: nginx-app-3
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-app-3
  template:
    metadata:
      labels:
        app: nginx-app-3
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```

Now that our Manifest files are ready, we need to create ArgoCD Applications to point to those Manifests.

ArgoCD can be configured in three different ways: using the GUI, using the CLI, or using Kubernetes Manifest files. We will use the third method.

Create the following Manifest files in a new folder `argocd/argo-apps`.

This is `argocd-apps/app-1.yml`:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app-1
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: argocd
    server: https://kubernetes.default.svc
  project: default
  source:
    path: argocd/my-apps/app-1
    repoURL: https://gitlab.com/ayush-sharma/example-assets.git
    targetRevision: HEAD
```

This is `argocd-apps/app-2.yml`:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app-2
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: argocd
    server: https://kubernetes.default.svc
  project: default
  source:
    path: argocd/my-apps/app-2
    repoURL: https://gitlab.com/ayush-sharma/example-assets.git
    targetRevision: HEAD
```

And this is `argocd-apps/app-3.yml`:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app-3
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: argocd
    server: https://kubernetes.default.svc
  project: default
  source:
    path: argocd/my-apps/app-3
    repoURL: https://gitlab.com/ayush-sharma/example-assets.git
    targetRevision: HEAD
```

As you can see, we are creating a Kubernetes object called `Application` in the `argocd` namespace. This object contains the source Git repository and destination server details. Our Applications are pointing to the Nginx manifest files we created earlier.

## Configuring our main application

Now we need some way to tell ArgoCD how to find our three Nginx applications. We do this by creating yet another Application! This pattern is called the `App of Apps` pattern, where one Application contains the instructions to deploy multiple child Applications.

Create a new Application from the GUI called `my-apps` with the following configuration:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-apps
spec:
  destination:
    namespace: default
    server: 'https://kubernetes.default.svc'
  source:
    path: argocd/argocd-apps
    repoURL: 'https://gitlab.com/ayush-sharma/example-assets.git'
    targetRevision: HEAD
  project: default
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

After creation, `my-apps` will begin syncing on the GUI:

{% include article-image.html src="automating-argocd-with-argocd-main-app.png" alt="Automating ArgoCD with ArgoCD! - Main app." %}

After the sync is complete, our three Nginx applications will appear on the GUI as well:

{% include article-image.html src="automating-argocd-with-argocd-dashboard.png" alt="Automating ArgoCD with ArgoCD! - Dashboard." %}

Since we did not enable `AutoSync`, manually sync `app-1`, `app-2`, and `app-3`. Once synced, our Nginx replicas will be deployed for all three apps.

{% include article-image.html src="automating-argocd-with-argocd-deployment.png" alt="Automating ArgoCD with ArgoCD! - Deployment." %}

## Conclusion

Mastering the `App of Apps` pattern is critical to leveraging the full power of ArgoCD. This method allows us to manage groups of applications cleanly. For example, deploying Prometheus, Grafana, Loki, etc., could be managed by a DevOps Application, whereas deploying frontend code could be managed by a Frontend Application. Configuring different sync options and repo locations for each allows more granular control over different application groups.

Happy coding :)