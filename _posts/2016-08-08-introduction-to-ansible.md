---
layout: post
title:  "Introduction to Ansible"
number: 2
date:   2021-08-21 12:00
original_date: 2016-08-08 03:00
excerpt: "Ansible is the answer to custom scripts and tooling that eventually snowball into a kludge which breaks more things than it fixes."
Category: Technology
---
I've worked in IT for a long time, and my secret collection of shell and Python scripts has made a lot that work easier. I remember having a repository full of scripts to install this, automate that... you get the idea. Some of these have been my pride and joy, full of witty remarks, inside jokes, and pop culture references. And I've handed these down to teams that succeeded me with pride. But these DIY adhoc scripts don't scale well, and often cause more trouble than they're worth.

Ansible is the tool to solve these problems. It's a collection of several (hundred?) modules to do most of what you can imagine doing as a sysadmin/troubleshooter. You can assemble these modules into Playbooks and use them for adhoc maintenance or reliable production use-cases. Say you need to set up a new Linux machine with Apache installed. In bash you would write the installation commands one by one. But in Ansible, you can create a task for what you want and let Ansible take care of the rest. That's the key difference: moving from **how** to **what**. With Ansible, you define the final state of your system and Ansible manages the execution - this is the difference between **declarative** and **imperative**.

The Ansible command for installing Apache would look something like this.

```yaml
apt: name="apache2" state="present"
```

The above command uses `apt-get` to install the apache2 package if it is not already installed. The command is easier to read, easier to review, and easier to diagnose in case of errors. It makes life easier for you when you need to review things six months down the road, and new team members can understand what's going on that much faster.

## Installing Ansible
The Ansible website has detailed instructions on [how to install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

## Trying It Out
Let's give Ansible a go.

Ansible works by uploading modules to the target machine and then executing them. This makes Ansible agent-less.

Set up a [Vagrant]({% post_url 2016-08-13-introduction-to-vagrant %}) machine with IP 192.168.1.20, or replace 192.168.1.20 with the IP of any machine you can access over SSH. If you're taking the Vagrant route, you can use this Vagrantfile:

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/hirsute64"
  config.vm.box_version = "20210820.0.0"
  config.vm.network "private_network", ip: "192.168.1.20"
end
```

Next, create a file called `hosts` in your current directory with the following contents:

```plaintext
[myhost]
192.168.1.20

[myhost:vars]
ansible_ssh_user=vagrant
ansible_ssh_private_key_file=/private/tmp/a/.vagrant/machines/default/virtualbox/private_key
```

The `ansible_ssh_private_key_file` is the SSH key file for your vagrant box which Ansible will need to make the connection. You can find your key path using `vagrant ssh-config`.

Now run `ansible -i hosts myhost -m "ping"`. Your output should be:

```shell
192.168.1.20 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

And it's that simple! Let's try another one.

```shell
ansible -i hosts myhost --become -m "apt" -a "name=nginx state=present"
```

The above command executes [Ansible's apt module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html) to install Nginx on the Vagrant box. `--become` allows Ansible to execute as root on the target box, `-m apt` calls the `apt` module, and `-a` specifies the arguments for the module. Ansible has a huge library of modules for automating even the most complex tasks. And once you're comfortable with adhoc commands, you can explore [Playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html) - which are like entire recipes containing adhoc commands to execute in one go :)

Check out the Ansible website for an [exhaustive list of modules it supports](https://docs.ansible.com/ansible/latest/collections/index_module.html). Ansible has been around for a long time, so can always find help on StackOverflow and other websites.

## Key points to remember:

1. Ansible is agent-less: There is no need to install special software on target systems.
2. Ansible is declarative: Describe your target system and let Ansible do the rest.
3. Since everything executes over SSH there's no extra technology involved. Meaning fewer moving parts to manage and worry about.
4. Ansible modules are more readable than code, meaning code reviews are much easier.
5. Does your infrastructure have tech stacks from more vendors than you can handle? Among Ansible's hundreds of modules you'll find the one that you need.
6. Ansible is clean, simple, and fast, and doesn't need complex databases to work. So there's no need to maintain Ansible itself.

There are a lot of resources on Ansible, so be sure to check them out. The Ansible website has a lot of case studies and white papers for you to learn more.

## Resources
- [https://www.ansible.com/](https://www.ansible.com/)
- [https://www.ansible.com/case-studies](https://www.ansible.com/case-studies)
- [http://stackoverflow.com/questions/tagged/ansible](http://stackoverflow.com/questions/tagged/ansible)
- [https://groups.google.com/forum/#!forum/ansible-project](https://groups.google.com/forum/#!forum/ansible-project)