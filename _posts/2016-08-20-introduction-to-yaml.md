---
layout: post
title:  "Introduction to YAML"
number: 14
date:   2021-08-21 03:00
original_date:   2016-08-20 00:00
excerpt: "What is YAML and why is it about time we started using it?"
Category: Technology
---
[YAML](http://yaml.org/) (YAML Ain't Markup Language) is a human-readable data serialization language. Its syntax is simple and human-readable. It does not contain quotation marks, opening and close tag, or braces. It does not contain anything which might make it harder for humans to parse nesting rules. You can scan your YAML document and immediately know what’s going on.

## Killer features
YAML has some killer features which make it superior to other serialization formats.

- Easy to skim.
- Easy to use.
- Portable between programming languages.
- Native data structures of Agile languages.
- Consistent model to support generic tools.
- Supports one-pass processing.
- Expressive and extensible.

Let me show you YAML's power further with some examples.

Can you figure out what's going on below?

```yaml
-------
# My grocery list
groceries:
     - Milk
     - Eggs
     - Bread
     - Butter
...
```

The above example contains a simple list of groceries to buy, and it’s a fully-formed YAML document. In YAML, strings aren't quoted and lists need simple hyphens & spaces. A YAML document starts with `---` and ends with `...` but they are optional. Comments in YAML start with a #.

Indentation is key in YAML. Indentation must contain spaces, not tabs. And while the number of spaces required are flexible, it's a good idea to keep them consistent.

## Basis Elements

### Collections
YAML has two types of collections: lists (for sequences) and dictionaries (for mappings). Lists are key-value pairs where every value is on a new line, beginning with a hyphen and space. Dictionaries are key-value pairs where every value is a mapping contain a key, a colon and space, and a value.

For example:

```yaml
# My List
groceries:
     - Milk
     - Eggs
     - Bread
     - Butter

# My dictionary
contact:
 name: Ayush Sharma
 email: myemail@example.com
```
Lists and dictionaries are often combined to contain more complex data structures. Lists can contain dictionaries and dictionaries can contain lists.

### Strings
Strings in YAML don't need quotation marks. Multi-line strings are  defined  using | or >. The former preserves newlines but the latter does not.

For example:

```yaml
my_string: |
     This is my string.
     It can contain many lines.
     Newlines are preserved.
```

```yaml
my_string_2: >
     This is my string.
     This can also contain many lines.
     Newlines aren't preserved and all lines are folded.
```
### Anchors
YAML can have repeatable blocks of data using node anchors. The "&" character defines a block of data that later referenced using "*". 
For example:

```yaml
billing_address: &add1
 house: B1
 street: My Street

shipping_address: *add1
```

At this point, you know enough YAML to get started. You can play around with the [online YAML parser](https://yaml-online-parser.appspot.com/) to test yourself. If you work with YAML daily then [this handy cheatsheet](http://yaml.org/refcard.html) will be helpful.