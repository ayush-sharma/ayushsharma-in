---
layout: post
title:  "A guide to better town management in Caesar III"
number: 102
date:   2005-07-18 00:00
excerpt: "Caesar III can be challenging, even more so if your town is not productive."
Category: "Video Games"
banner_img: "2005-07-18-a-guide-to-better-town-management-in-caesar-iii.jpg"
---
Caesar III is a good game, unlike a few others in it's genre. The concept of building Rome from scratch, and slowly rising to the position of power, adds a zing to the game like never seen before. Pure power, total control. Building Colosseums to please your people, temples to please the Gods. But since you'll be in control of the lives of hundreds, maybe thousands, of people, it's very easy to mess up. Everybody needs water, entertainment, access to temples, and with limited space, this becomes difficult. Here are a few ways/rules which will make sure your empire runs as smoothly as possible.

<div class="row">
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-18-a-guide-to-better-town-management-in-caesar-iii-1.jpg"
            alt="Caesar III tips" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-18-a-guide-to-better-town-management-in-caesar-iii-2.jpg"
            alt="Caesar III tips" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-18-a-guide-to-better-town-management-in-caesar-iii-3.jpg"
            alt="Caesar III tips" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-18-a-guide-to-better-town-management-in-caesar-iii-4.jpg"
            alt="Caesar III tips" %}
    </div>
</div>

1. Build roads branched, so that you can access all of them easily later. For example, from one main road, create a few more to the right and left. On these right and left "sub-roads", you can build your actual houses, wells, gardens, etc. Nothing more. At the two ends of the main road, build your Senate and a market (figure 1). Be careful, though. If the main road is too long and has too many branches, your citizens might keep strolling around this maze itself, and never reach places where labor is required. Keep this 'main road - sub-road' combo short and small, with a max of 20 houses, or four branches. The concept is that with a lot of houses connected together, you can handle them as separate sections in your empire.
2. When you have a lot of sections in your empire, let each section have different places of entertainment and religion. Separate schools, separate baths, etc. This way you'll be sure none of the houses deteriorate, and your available workforce will be utilized to he maximum.
3. Another interesting thing I've seen gamers do is divide the entire map into sections! One part of the map will only have houses, the other will only have entertainment hubs and temples, another will have all the markets and granaries. And then all the three are connected, such that the people can access the temples and the granaries, but the granary can't access the temple. The point here is to allow access only to the required places. With a lot of markets in one place, and lot of houses in another, access to markets will be maximum. 
4. Keep clay pits, workshops and other labor-intensive buildings away from your residential area. This is actually very logical: would you like a weapons workshop next door, hammering away at the metal? I didn't think so. Again, you can give all your workshops and raw material sources a different section on the map, and connect them to markets.
5. This is important. Build only farms near sources of food, not houses. If you've played the demo, recall the second mission, with a wheat patch to the south of the map, bordered by immovable rocks (figure 2). Here's what some people do: they build entire housing colonies in the food patch, and leave no place for more farms. When they start running out of food, they have no choice but to remove these houses, and place more farms. When the food patch is enclosed by some immovable obstacle, you have no choice but to utilize that whole space for food. When this patch is in the open, place farms on the patch, and houses around it, to make sure it has access to labor. In the map in consideration (figure 2), place a lot of farms, a granary just outside this rock enclosure, and a few houses in between, to make sure it has labor.
6. The number of farms and your population should be balanced. If you have more farms and less people, then your public will eat more food than they grow, and you will get constant warnings to indicate this. If you have more people, but less farms, your people will obviously starve. To make sure none of this happens, keep a constant check on your granaries: if the granary is empty, you need more farms, if the granary is full, you need to shut down a few farms. Keep a check on your warehouses, as well. This is where surplus food goes, when your granaries are full.
7. You don't need too many houses to keep your population growing. Just ten houses with all the amenities it requires is enough to support quite a lot of people. Keep right-clicking on houses from time to time, to see what's hampering their evolution.
8. Hold festivals regularly, at least a large festival in six months. You can hold a grand festival when you think your public is losing faith in you, or when they're getting really bored. Be sure to dedicate equal number of festivals to each deity, and make equal number of temples to each, so that you don't lose their favor. Check the 'Religion' tab of the Senate to see your standing with the Gods.
9. If your population rises too high, evict somebody!

Caesar III isn't a tough game. What you do in the game is what the Senators themselves had to do in Rome, thereby making this game more lifelike. In the end it seems as if your people control you, not the other way around, but you must keep emigration and immigration in check at all times, to make sure your population doesn't get out of hand. Play this game right, construct and plan right, like the great architects of Rome, and you'll be fine. Aesthetics and Practicality are the keys to a good Rome.