---
layout: post
title:  "AWS EBS Types Read/Write Benchmarks"
number: 24
date:   2017-01-19 16:00
excerpt: "I ran hdparm and dd on GP2, PIOPS, SC1, ST1, and Magnetic EBS volumes. Check out the results."
Category: Technology
---
I wanted to run benchmarks on all the AWS EBS disk types for reference. I created a machine with the following disks:
{% include article-image.html src="aws-ebs-disk-configuration-for-read-write-benchmarks.jpg" width="700" height="255" alt="Disk configuration for EBS read/write benchmarks." %}

## Scripts
Disk reads were done using the following script:

```bash
#!/bin/bash
for i in {1..5}; do hdparm -Tt /dev/xvda; done
for i in {1..5}; do hdparm -Tt /dev/xvdb; done
for i in {1..5}; do hdparm -Tt /dev/xvdc; done
for i in {1..5}; do hdparm -Tt /dev/xvdd; done
for i in {1..5}; do hdparm -Tt /dev/xvde; done
for i in {1..5}; do hdparm -Tt /dev/xvdf; done
for i in {1..5}; do hdparm -Tt /dev/xvdg; done
```

Disk writes were done using the following script:

```bash
#!/bin/bash
for i in {1..5}; do echo "-- xvda"; dd if=/dev/zero of=/dev/xvda bs=4k count=100k; done
for i in {1..5}; do echo "-- xvdb"; dd if=/dev/zero of=/dev/xvdb bs=4k count=100k; done
for i in {1..5}; do echo "-- xvdc"; dd if=/dev/zero of=/dev/xvdc bs=4k count=100k; done
for i in {1..5}; do echo "-- xvdd"; dd if=/dev/zero of=/dev/xvdd bs=4k count=100k; done
for i in {1..5}; do echo "-- xvde"; dd if=/dev/zero of=/dev/xvde bs=4k count=100k; done
for i in {1..5}; do echo "-- xvdf"; dd if=/dev/zero of=/dev/xvdf bs=4k count=100k; done
for i in {1..5}; do echo "-- xvdg"; dd if=/dev/zero of=/dev/xvdg bs=4k count=100k; done
```

## Results

### Instance Type: m3.medium Read Speeds (MBPS)
```plaintext
Mount Point	/dev/xvda	/dev/xvdb	/dev/xvdc	/dev/xvdd	/dev/xvde	/dev/xvdf	/dev/xvdg
Disk Type	Root GP2	Instance Store	EBS PIOPS (IO1) 400	EBS PIOPS (IO1) 5000	EBS Cold HDD (SC1)	EBS TOHDD (ST1)	Magnetic
Read Type	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read
Reads	5027.25	83.99	5118.29	805.69	5083.22	83.24	5041.15	80.32	5067.19	55.79	5055.38	82.24	4982.88	83.18
5054.29	82.23	5045.46	816.08	5039.56	80.72	5081.32	79.91	5033.26	55.83	5030.27	84.34	4991.25	80.73
5099.39	83.3	5071.96	806.43	5059.23	83.94	5050.78	83.21	4992.19	55.82	5006.38	84.17	5017.09	80.66
5102.44	83.92	5078.28	803.88	5057.22	81.85	5037.17	81.68	5046.49	55.84	4962.85	84.05	5016.52	81.42
5055.4	83.29	5078.03	786.34	5055.63	83.99	5012.06	83.42	5127.5	55.84	4968.41	84.07	4984.07	80.83
Average Read	5067.754	83.346	5078.404	803.684	5058.972	82.748	5044.496	81.708	5053.326	55.824	5004.658	83.774	4998.362	81.364
```

### Instance Type: m3.medium Write Speeds (MBPS)
```plaintext
Mount Point	/dev/xvda	/dev/xvdb	/dev/xvdc	/dev/xvdd	/dev/xvde	/dev/xvdf	/dev/xvdg
Disk Type	Root GP2	Instance Store	EBS PIOPS (IO1) 400	EBS PIOPS (IO1) 5000	EBS Cold HDD (SC1)	EBS TOHDD (ST1)	Magnetic
Writes	1200	204	31.4	37.2	37.2	37.2	31.1
65.3	259	37.2	37.2	37.2	37.2	31.4
246	232	37.2	37.2	37.2	37.2	31.1
81.8	221	37.2	37.2	37.2	37.2	31.8
383	231	37.2	37.2	37.2	37.2	30.6
Average Write	395.22	229.4	36.04	37.2	37.2	37.2	31.2
```

### Instance Type: c3.8xlarge Read Speeds (MBPS)
```plaintext
Mount Point	/dev/xvda	/dev/xvdb	/dev/xvdc	/dev/xvdd	/dev/xvde	/dev/xvdf	/dev/xvdg
Disk Type	Root GP2	Instance Store	EBS PIOPS (IO1) 400	EBS PIOPS (IO1) 5000	EBS Cold HDD (SC1)	EBS TOHDD (ST1)	Magnetic
Read Type	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read	Cached Read	Buffered Read
Reads	11057.17	173.88	10893.09	953.87	11069.15	118.62	11035.86	329.37	11088.44	55.81	11052.17	116.44	10998.47	64.63
10988.7	173.88	11029.77	967.62	11032.89	118.62	11061.04	358.53	11055.34	55.66	11050.93	173.94	10995.74	90.13
11056.83	173.87	10939.08	951.2	11083.63	118.62	11067.88	358.36	11024.41	55.75	11076.77	174.03	10962.87	91.32
11103.1	173.88	10931.89	947.6	11061.23	118.62	11025.63	362.81	11048.92	55.7	11035.29	174.03	10934.36	90.62
11070.23	173.88	10960.17	889.17	11060.32	118.63	11028.91	358.85	11075.36	55.71	11021.72	99.11	10974.47	95.68
Average Read	11055.206	173.878	10950.8	941.892	11061.444	118.622	11043.864	353.584	11058.494	55.726	11047.376	147.51	10973.182	86.476
```

### Instance Type: c3.8xlarge Write Speeds (MBPS)
```plaintext
Mount Point	/dev/xvda	/dev/xvdb	/dev/xvdc	/dev/xvdd	/dev/xvde	/dev/xvdf	/dev/xvdg
Disk Type	Root GP2	Instance Store	EBS PIOPS (IO1) 400	EBS PIOPS (IO1) 5000	EBS Cold HDD (SC1)	EBS TOHDD (ST1)	Magnetic
Writes	2100	2100	114	417	47.8	182	28.6
2700	2800	106	333	43.9	137	35.1
2900	2900	106	332	43.8	137	33.6
2900	3000	106	332	43.8	137	34.1
2200	2200	106	332	43.8	137	34.8
Average Write	2560	2600	107.6	349.2	44.62	146	33.24
```