---
layout: post
title:  "Rise of the Argonauts review"
number: 103
date:   2009-06-08 00:00
excerpt: "Stunning level design, innovative combat system, and beautiful character models make this game worth your time and money."
Category: "Video Games"
banner_img: "2009-06-08-rise-of-the-argonauts-review.jpg"
---
Rise of the Argonauts follows the story of Jason and the Argonauts from Greek Mythology, though with quite a few changes which, all in all, make for an amazing gaming experience.

{% include article-image.html src="2009-06-08-rise-of-the-argonauts-review.png" alt="Rise of the Argonauts!" %}

The story of the game begins with Jason's wife Alceme being assassinated by a shadowy character, who you must then pursue to uncover his motives. Vowing to bring your one true love back to life, a trip to the Oracle at Delphi reveals that you must seek the Golden Fleece, which can revive Alceme. But in order to accomplish this, you must rally a group of heroes with divine blood in their veins. After that, it is revealed that the Golden Fleece lies in a place where none have ventured. The game has all the elements of a good story: famous heroes from all over Greek mythology, such as Hercules and Achilles, an ancient prophecy, a powerful antagonist, and much more. The story is revealed gradually through dialogues and quests which make it all the more gripping.

The character design in ROTA is crisp and well-detailed, and goes a long way in showing how much time and effort was put into this game. The gods and the Titans have been created masterfully and will be in your mind long after you are done with this game. The level design is rich, featuring varied and well-made terrains, ranging from lush, verdant jungles to the fiery, molten, and abysmal depths of Tartaurus itself.

The game also has a variety of god powers, though some of them are not as powerful. You gain these powers by gaining the favor of one of four gods: Ares, Hermes, Apollo, and Athena. As you complete quests and achieve certain objectives, you can dedicate the achievements, or your "deeds", to any one of the four gods, and once a specific number of deeds has been dedicated, you can use them to buy powers. You may favor more than one god, but which one you do will significantly effect the damage and health, among other things, of your character. You also gain favor by choosing god-specific dialog choices, which keeps things interesting.

The game has four weapons, each of which is favored by a particular god: mace by Ares, sword by Hermes, shield by Apollo, and spear by Athena. Spending deeds and gaining the powers of these gods will make you more proficient with that god's favored weapon. The combat system reminded me of King Leonidas from the movie 300, especially when you fight with the spear. Though innovative and very convincing, it does get a bit tedious towards the end, as irrespective of what weapon you choose, you'll be able to dispatch all enemies quickly as you become more powerful. The enemy AI is not that good, and they come at you in more or less predefined ways. The enemies are not very different, and are a combination of big or small, with shield or without. And that's about it as far as enemies are concerned, but thankfully the overall concept of the game is more than sufficient to keep you enthralled during this drudgery.

Excellent graphics, great storyline, amazing character and set design, good, though sometimes tedious, combat system make this game a must-have in my list.
