---
layout: post
title:  "Easy pretty URL redirects with Jekyll and Netlify"
number: 117
date:   2021-11-16 02:00
excerpt: "Combine Jekyll and Netlify to create pretty URL redirects as easy as 1-2-3."
Category: Technology
banner_img: "2021-11-16-easy-pretty-url-redirects-with-jekyll-and-netlify.jpg"
is_series: true
series_title: "Jekyll"
---
Pretty URLs, aka vanity URLs, aka tiny URLs, are great for improving SEO and the UX. They're shorter versions of a domain, making them useful for copy-pasting and printing. There are already plenty of domain shortening services out there. But with Jekyll and Netlify I can configure pretty URLs all by my lonesome!

What I want to achieve is as follows.

Every article on this website has a `number` in its Front Matter. I want to use these sequence numbers to redirect to the main post. Using numbers as pretty URLs will make it easier for users to type it into the address bar.

## The plan
My article on [Getting started with ArgoCD]({% post_url 2021-07-06-getting-started-with-argocd %}) is [article 94](https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/2.0/_posts/2021-07-06-getting-started-with-argocd.md#L4). So I want `https://<my-pretty-domain>/94` to redirect to the full article. Any other link for `https://<my-pretty-domain>` which doesn't contain a valid article number should redirect to the primary domain, `ayushsharma.in`.

By breaking down the plan into individual tasks, I get the following list:

1. Register pretty domain and point it to Netlify.
2. Redirect pretty URLs for articles to full URL.
3. Redirect all other pretty URLs to primary domain, `ayushsharma.in`.

## Register pretty domain and point it to Netlify
The first tasks is to get a pretty domain.

I've registered [ayush.to](https://ayush.to) as my  pretty URL and pointed it to Netlify. There are several options on how to [point a domain to Netlify's load balancers](https://docs.netlify.com/domains-https/custom-domains/configure-external-dns/#configure-an-apex-domain). I'm pointing `ayush.to`'s `A record` to Netlify which you can verify with `nslookup`.

```shell
> nslookup ayush.to

Server:		10.1.0.1
Address:	10.1.0.1#53

Non-authoritative answer:
Name:	ayush.to
Address: 75.2.60.5
```

Additionally, I've also added `ayush.to` as a [domain alias](https://docs.netlify.com/domains-https/custom-domains/multiple-domains/#domain-aliases) so Netlify can issue the SSL certs.

## Redirect pretty URLs for articles to correct URL.
Netlify offers two ways of handling redirections for static websites: [_redirects file](https://docs.netlify.com/routing/redirects/#syntax-for-the-redirects-file) and [Netlify.toml](https://docs.netlify.com/routing/redirects/#syntax-for-the-netlify-configuration-file). Both options use a different syntax for the redirection and provide different features.

Both options also have a specific [order of processing](https://docs.netlify.com/routing/redirects/#rule-processing-order). Netlify processes the `_redirects` file before `Netlify.toml`. So my plan is to use `_redirects` for the actual redirects and `Netlify.toml` for the catch-all.

To create the `_redirects` file I'll use the following code:

```liquid
{%raw%}---
---
{% assign posts = site.posts | sort: 'number' %}
{% for post in posts %}{{ post.number | prepend: "https://ayush.to/" }}    {{ post.url | prepend: site.baseurl | prepend: site.url }}
{% endfor %}{%endraw%}
```

Jekyll [ignores files beginning with an underscore (`_`)](https://jekyllrb.com/docs/structure/) by default. So we'll need to include it in the `_config.yml`.

```yaml
include:
  - _redirects
```

After the build process completes, the `_redirects` file in the destination folder looks like this:

```plaintext
https://ayush.to/0    https://www.ayushsharma.in/2016/08/hello-world
https://ayush.to/1    https://www.ayushsharma.in/2016/08/douglas-noel-adams
https://ayush.to/2    https://www.ayushsharma.in/2021/08/introduction-to-ansible
https://ayush.to/3    https://www.ayushsharma.in/2016/08/json-web-tokens
https://ayush.to/4    https://www.ayushsharma.in/2016/08/nano-keyboard-shortcuts
https://ayush.to/5    https://www.ayushsharma.in/2016/08/introduction-to-fluentd
...
```

If there were no syntax errors in the `_redirects` file, Netlify should confirm this by telling you the number of redirect rules it processed.

{% include article-image.html src="2021-11-16-easy-pretty-url-redirects-with-jekyll-and-netlify-log.jpg" width="1024" alt="Netlify logs showing the total number of redirect rules processed." %}

I'm almost there! I've generated the `_redirects` file. Now to handle the catch-all.

## Redirect all other pretty URLs to primary domain
My `_redirects` file can handle specific redirections. But I also need to set a catch-all.

This is where `Netlify.toml` comes in. In `Netlify.toml`, I'm going to tell Netlify to redirect everything to my primary domain. `Netlify.toml` comes after `_redirects` in the order of processing. Meaning my catch-all only kicks in if no specific redirection rules matched. Nice!

My `Netlify.toml` now contains the following code:

```toml
## ayush.to
[[redirects]]
  from = "https://ayush.to/*"
  to = "https://www.ayushsharma.in"
  status = 301
  force = true
  [redirects.headers]
    X-From = "Netlify"

[[redirects]]
  from = "https://www.ayush.to/*"
  to = "https://www.ayushsharma.in"
  status = 301
  force = true
  [redirects.headers]
    X-From = "Netlify"
```

## The test drive!
Now let's see if this whole thing even works.

I've pushed the above code to Netlify and the robots have completed their task.

So. If I go to [ayush.to/94](https://ayush.to/94) I should see the article on "Getting started with ArgoCD".

And...

```shell
> curl https://ayush.to/94

Redirecting to https://www.ayushsharma.in/2021/07/getting-started-with-argocd
```

... it works!

## Conclusion
Pretty URLs are a powerful feature in a tiny package. And I was able to set it up neatly and quickly thanks to Jekyll and Netlify.

The ability to configure Netlify's robots using simple scripts is why I love the service so much. There are of course plenty of improvements we can make. We could improve handling of `www` and non-`www` versions of domains. Instead of using numbers as in this example, we could also use smaller slugs or post tags and categories. We can set one-off redirects for special occassions like sales or book launches. The list goes on and on.

I hope the wheels in your head have begun turning. Let me know if you come up with a crazy new use for the Netlify redirection robots. Good luck, and happy coding :)