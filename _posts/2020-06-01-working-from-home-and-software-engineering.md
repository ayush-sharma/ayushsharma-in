---
layout: post
title:  "​​Working From Home and Software Engineering"
number: 89
date:   2020-06-01 00:00
excerpt: "Your time and attention is your primary resource, so deploy it wisely."
Category: Technology
---
I've been working from home for over ten weeks now.

It was comfortable in the beginning.

In February, the WFH seemed temporary, some small adjustment before we could return to our regular lives. It didn't take long for the abnormal to become the norm.

Companies have started exploring permanent work-from-home solutions, and many have said they won't recall employees for 10-18 months, or at least until there is a vaccine.

I have an SRE/DevOps role at work, and I'm trying to figure out how to navigate this new reality. The rules don't seem very clear, and when they are clear, it's hard to establish a shared understanding.

I don't have clear solutions, but this is where my head is at...

## 1.
## I'm not where everyone else is.

WFH means that I'm the master of my own little office. This increased autonomy means I can personalise how I work. But not being in the same physical space as everyone else has created new challenges.

One pronounced disadvantage is that I can't send visual cues anymore. It was easy to communicate that I'm out for lunch or taking a break if I wasn't at my desk. Or if I was packing my things around 5PM, it meant I didn't have time for new work. Sitting face-to-face in the office made it easy to know who was occupied and who was free enough to have a quick discussion on something. In the absence of these clear signals, I end up imagining what someone might be doing, and I reach out to them directly if I think they're free. This can end up being distracting and can break their train of thought, which forces them to either ignore me or deal with me now that I've interrupted them.

Dealing with operational issues has become harder now that I'm not in the same room with my team, and it's hard to quickly exchange to resolve the operational problems. This is somewhat fixable with video-conferencing, but I'll get to that in a minute. Simple brainstorming has also suffered. Discussing ideas is much harder in online meetings since we're all just a bunch of floating heads, talking, and floating heads can't collaborate on a white-board. Screen-sharing and tablet/stylus combos can help, but not everyone has those.

## 2.
## I'm not when everyone else is.

At home, we all keep various schedules based on our living situation. Keeping plans in sync as a team can be tricky since you need at least some amount of overlap for meetings/sync ups if you're going to get anything done. Defining clear boundaries and timelines makes all the difference between retaining control of time and delivering something of value at the end of the day/week/month. And if I don't define my boundaries, someone else will do it for me, so communicating these is key.

This is helpful to do for the team and the rest of the organisation as well. You may know your peers well, but when you need to reach out to someone outside your usual circle, having a protocol defined in advance reduces distractions for everyone. Asynchronous communication is of utmost importance, and if you do need to speak with someone, having clear guidelines can help.

## 3.
## Video-calling is a shitty substitute.

Video meeting tools like Zoom/Slack/Hangouts has been a hit or miss at making me feel comfortable.

The little yellow light feels Orwellian, especially if you consider that the companies behind these apps have shitty track records when it comes to privacy. I also feel a palpable sense that something is "on", that there is a software or an app which is part of the conversation as well, mediating the exchange of words over wires and air.

Before COVID, I would use these apps for official meetings. So I've gotten used to keeping an eye on the clock to keep things professional which makes it harder to just let loose and talk to someone. One thing that has worked for me is to keep video apps for personal conversations separate. This helps me draw work/life boundaries in the online realm. So Whatsapp for personal calls, Slack/Zoom for office things. It's... working-ish.

Video calling also introduces a new problem: lag. Real-world conversations are lag-free, which is a ridiculous thing I never thought I would have to think about. It's hard enough in video calls to not speak over each other with a dozen floating heads around you. With lag and poor connectivity, the conversation can become impossible.

Cameras create another problem. In real-life, you had to be seen when you spoke. It makes sense then for us to have video turned on when we're in meetings. Many people choose not to do this for one reason or another, which makes it impossible to have a conversation. I can't pick up on body language or non-verbal cues in a video call, and with cameras turned off, not talking over other people is next to impossible.

A lot of this can be fixed by establishing clear guidelines around communication. There are at least a few situations where you need to take a hard stand and ensure people can see each other. For example, if you're having a one-on-one discussion with your mentor/mentee or if you're conducting an interview. But when and where this becomes mandatory or not must be communicated clearly.

## 4.
## I have too many non-shitty substitutes.

Sure sure, we have all these amazing apps to keep us connected. We have slack and zoom and skype and hangouts and phones and project management apps and time management apps and productivity apps and phones and super apps to manage other apps. But who's going to keep all of these talking to each other? How does someone giving me a phone call know that I cancelled because I'm on slack and not dissing their request? What's the etiquette for cancelling an incoming hangout or phone call because I'm zoomcalling (zooming? Is it a verb yet?)

What if I just don't feel like hanging out or zooming or slacking. As I write this, I have a strange feeling that some software engineer is building the-one-app-to-rule-them-all, but the answer to our tech problems has rarely been more tech. I don't think this is a tech problem. We just need to define and get used to a different set of social etiquette when communicating online. Every. Single. Day.

## 5.
## I have too many or too few dashboards to look at

It's hard to segregate specific time away for useful meetings when anyone can just pull you in using your calendar. Commanding your own time can be the difference between being productive at work and not knowing what you did at the end of the day. To maintain a regular schedule, set clear boundaries, and get your team in the loop.

One easy way to do this is dashboards. SRE/DevOps teams already have too many to view different server metrics, escalations, product metrics, project management views, etc. This is an excellent time to cut the dashboard clutter and organise and consolidate what you can and delete the rest. We also need to identify where we need instant visibility to avoid useless status meetings to get situational awareness directly and on our own time.

## 6.
## Support communication is essential.

For SRE and Support teams working from home, the difference between urgent and business-critical communication can quickly begin to fade. Tech teams might reach out for queries on the status of the company VPN service or to check why their SSH might be failing. Having a status page display the state of all running services and a ready knowledge-base with initial debugging steps for common issues can be a huge time-saver.

Productivity gets hampered ultimately by how much we context-switch, and reducing the number of incoming tickets and queries can make a huge difference. This was important before our current WFH situation also but its doubly important now that we lack the discipline and the networking infrastructure that an office previously provided.

It will be prudent to schedule weekly reviews of support tickets and create knowledge-bases and support articles to help those who might face the problem in the future. The time you save this way is intangible. Its benefit is incalculable. But it is also invaluable because the time you save in explaining what you already know can be used to recharge after a long day. If you're using a documenting platform like Atlassian or an internal blog/wiki, now might be the time to start writing more and debugging less. And get an effing blog together.

There are other ways to share knowledge. Mac has screen-recording software built-in via QuickTime, and there are free and open-source alternatives available for other OSes as well. Why not demonstrate a process or a product demo in the comfort of your home and share it with a broader audience? Video recordings like these are an excellent way to communicate asynchronously. They will augment your knowledge-base by providing video guides for people who respond to videos more than text, and you don't need to keep MoMs because the Ms are right there.

## 7.
## Our production servers don't give af about our schedules.

Production servers, like babies, need things at the least appropriate time. We can't control when they need attention, but we can control if they need care at all, and if they do, then who should get involved (mom? dad? neighbour?). Immutable infrastructure via golden images (Packer/Docker) can help you create throwaway infrastructure with minimal maintenance. In the event humans need to get involved, having a clearly documented communication strategy is essential. Don't just configure SNS alerts for everything that makes sense and call it a day. But define your alert strategy around SLAs that need to be met and have a clear escalation strategy built around that.

Tools like ServiceDesk, OpsGenie and PagerDuty can help, but only if you implement a well-thought-out communication plan. Alert strategies and channels should be designed to save people their time and protect production applications, but make that trade-off considerately.

Our time and attention is our primary resource, so deploy it wisely.