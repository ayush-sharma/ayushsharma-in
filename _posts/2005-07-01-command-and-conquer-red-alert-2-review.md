---
layout: post
title:  "Command and Conquer: Red Alert 2 review"
number: 100
date:   2005-07-01 01:00
excerpt: "A review of Command and Conquer: Red Alert 2, one of the best RTS games of all time."
Category: "Video Games"
is_series: true
series_title: "Command and Conquer"
---
When I sat down to write this little piece, I wanted to shout at the top of my voice, fill the air with foul utterances, and tell you to play this goddam game no matter what. And my failure to contain my excitement leads me to this: I am shouting already, my neighbours are looking out their window, and now I want you to "PLAY THIS GODDAM GAME!"

<div class="row">
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-01-command-and-conquer-red-alert-2-review-chronostorm1.jpg"
            alt="Chrono Legionnaires: the quickest way to make your enemy history" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-01-command-and-conquer-red-alert-2-review-chronostorm2.jpg"
            alt="The foolishly placed Soviet base, all for you to capture!" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-01-command-and-conquer-red-alert-2-review-chronostorm3.jpg"
            alt="LET 'EM HAVE IT!" %}
    </div>
    <div class="col-12 col-md-4 p-1">
        {% include article-image.html src="2005-07-01-command-and-conquer-red-alert-2-review-chronostorm4.jpg"
            alt="Soviet Nuke + Allied WCD = APOCALYPSE NOW!" %}
    </div>
</div>

Command and Conquer: Red Alert 2 review is one of those games that you want to sit and play for hours at a stretch, and not move or even eat, or go to the bathroom, which you eventually won't if you don't eat. This RTS (real-time strategy) is based, from what I gather, somewhere in or around the second World War, and this time, it's the Soviets and the Allies who are at it (no surprise)...

The part of the game that completely defeats this time period is the fact that you get to build teleportation devices, weather control devices, Iron Curtains for invulnerability, and the infamous Nuclear Missile silo. I'll be shouting some more at the end of this article on how cool all these are, but right now, let's get on with it.

You can play the game's campaign as an Ally or as a Soviet, both leading to the eventual defeat of the other, depending on which side you choose. The story unfolds differently for each side, but the underlying plot remains the same: while playing as the Allies, you might have to capture a base, but playing as Soviets, you need to defend it. And it's not like the roles get exactly reversed. The sequence of events that ultimately lead to your goal will really make you think at how different the two sides of the campaign really are. The units and buildings for the two sides are entirely different, obviously, and each has it's own strengths and weaknesses, which really makes to think in a way only a real RTS can.

I can't take it anymore. I REALLY must tell you about the campaign. It's the most awesome, most breath-takingly created I have ever seen. The cut-scenes are just beautiful. They feature actual actors rather than an animated version of people, moving their lips. The performances of all of them are stunning, and you'll wonder at how much more money these guys (and gals) make compared to Tom Cruise. Originality at it's best. You have to see it to believe it. Trust me. Playing as Allies, you get to meet President Dugan, Albert Einstein himself, a General Carville who's in charge of the operation against the Soviets, to name a few. You also meet Lt. Eva who will direct you in your mission, and who also says things like "Construction complete" and "Weather Control Device ready" when you're playing. There is a lot more in the cut-scenes, but since I personally like the Soviets more, I think I'll talk about them now. The Soviet part of the campaign is just as exciting as the Allied, and the cut-scenes more so. Again, pure genius. Apart from seeing Premiere Romanov and a lot more folks, I must especially mention that you meet a guy named Yuri. The way he speaks will actually make you consider the gravity of the situation you're put in. As a Soviet, you will be guided by Lt. Zofia. She has in-game voices, too, but I feel that "Nuclear Missile launched" is better than "Lightning storm created" any day, no matter who says it!

The skirmish game allows you to choose from any of the Soviet or Allied contries, and play against up to 8 opponents. You can set the difficulty of the game, whether your MCV is allowed to repack or not, and whether crates should appear or not, among other things. Crates are randomly placed on the map, here and there, and they sometimes give you money, and sometimes give you whole MCVs as well! You can even turn off the super-weapons, if you wish.

For super-weapons, the Allies obviously have more advanced stuff. The Weather Control Device can conjure up lightning storms for major damage, and the Chronosphere can teleport units anywhere on the map. It can even send enemy units to the bottom of the ocean! I think the Soviet weapons are much more functional. The Nuke can hurt plenty, and leaves it's target area filled with radiation, so that unsuspecting victims who crawl over are damaged beyond repair, or killed instantly.

Multiplayer is not all that much different from the skirmish, except for the human-opponent factor, and the ability to have teams. Again, the maps only support up to 8 players.

All in all, I think everybody should have this game. If you're not going to play, just have someone unlock the cut-scenes for you. It's just as great.