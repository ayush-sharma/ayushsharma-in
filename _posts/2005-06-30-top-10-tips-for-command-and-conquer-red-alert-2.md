---
layout: post
title:  "Top 10 tips for Command and Conquer: Red Alert 2!"
number: 98
date:   2005-06-30 02:00
excerpt: "Having trouble with Command and Conquer: Red Alert 2? Here are a few basic beginner things you should remember while playing."
Category: "Video Games"
is_series: true
series_title: "Command and Conquer"
---
1. The Allied GIs are really helpful. Select about five of them, hit 'D' to Deploy them, and your base is pretty much protected against all land-based attacks. Deploying five or ten GIs at the four corners of your base is pretty helpful, because not only are deployed GIs powerful, but they become Elite (three down-pointing arrows/chevrons) very easily. Just make sure they don't get run over by tanks...
2. If your MCV can be moved around, use it to your advantage to construct the War Factory and Nuclear Reactor away from your base.
3. The extremely long range of Prism Tanks makes them the most versatile tanks in the game. Their disadvantage is that their armor isn't very strong, so even a well-placed GI or Conscript can take them out.
4. Playing as Soviets? Your arsenal is not complete without a few Apocalypse Tanks! A pack of five of these killing machines is enough to destroy an entire enemy base.
5. If you're playing against a computer opponent, notice two things: firstly, they construct their base the exact same way all the time, and secondly, they will always target your War Factory with their Nuke or Weather Control Device. Sell your War Factory as soon as the enemy Nuke or WCD gets activated.
6. If you can manage, putting Soviet Tesla Troopers inside Allied IFVs is a deadly combo. Two of these are a match for Apocalypse Tanks. Put Engineers in IFVs to make an Engineer-IFV that can repair your units on the spot. Putting Tanya or a Seal inside an IFV gives them more range, aside from mobility.
7. Using the Iron Curtain on troopers, like Conscripts, GIs, Spies, Tanyas, Seals, or dogs, kills them instantly. Same goes for using the Chronosphere on these units.
8. Ever heard of "Chrono-drowning"? Use the Chronosphere on Tanks and other deadly units, and throw them in the water! Or, use it on water units like Destroyers or Carriers and put them on land! Mix and match, bring your enemy down to his knees with style! (Not sure if this trick works on Submarines, Dolphins or Squids).
9. Again, if your opponent is computer-controlled, like in skirmish games, then they will construct their Superweapons (Iron Curtain, Weather Control Device, Nuke Silo, Chronosphere) always towards one corner of the base. If you're playing as an Ally, have two Chrono-Legionnaires handy. Move these Legionnaires towards the enemy Superweapons, and REMOVE it right from under their noses!
10. Infiltrating an enemy War Factory with a Spy upgrades all your War Factory units. Infiltrating a Soviet Battle Lab allows you to make a Chrono Ivan, and infiltrating an Allied Battle Lab gives you Chrono Seals (although I'm not sure they're called that).