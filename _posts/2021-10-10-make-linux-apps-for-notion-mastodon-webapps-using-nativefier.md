---
layout: post
title: "Make Linux apps for Notion, Mastodon, or any web app using Nativefier"
number: 112
date: 2021-10-10 03:00
excerpt: "Notion is an awesome life manager. And Mastodon is great for decentralised internet shenanigans. With Nativefier, I can now enjoy them as apps on Ubuntu 20.04."
Category: Technology
banner_img: "2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-banner.png"
is_pinned: true
---
I use [Notion](https://notion.so) and [Mastodon](https://mastodon.technology) every day, and Ubuntu has been my desktop of choice for the last few years. I prefer dedicated apps for my daily drivers so I've had to use the browser to access Notion and Mastodon for now.

Thankfully I wasn't the only one who thought Linux faced a dearth of dedicated apps. And someone managed to do something about it. [Nativefier](https://github.com/nativefier/nativefier) takes a URL and wraps it into an Electron app running Chromium under the hood for Windows, Mac, or Linux. And using Nativefier is as simple as that.

## Installing Nativefier

Installing Nativefier is as simple as running:

```shell
npm install -g nativefier
```

On my Ubuntu, I had to upgrade my NodeJS. Nativefier requires `NodeJS >= 12.9`. Once installed, you can check your version of Nativefier.

```shell
nativefier --version
45.0.4
```

Running `nativefier --help` will list all options the app supports. I'm going to be using these to create apps for Notion and Mastodon below. Before we create our apps, I'm going to create a new folder called `~/NativeApps`. This will help me keep the the apps nice and organised.

```shell
mkdir ~/NativeApps
cd ~/NativeApps
```

## Creating an app for Notion

We'll start by creating the app for Notion. The command is as follows:

```shell
nativefier -n Notion -p linux -a x64 --width 1024 --height 768 --tray --disable-dev-tools --single-instance https://notion.so
```

The CLI options above do the following:

- `-n Notion` sets the app name to `Notion`.
- `-p linux -a x64` sets the app's platform to `Linux` and architecture to `x64`.
- `--width 1024 --height 768` set the apps dimensions on launch.
- `--tray` creates a tray icon for the app.
- `--disable-dev-tools` disables Chrome dev tools.
- `--single-instance` only allows one instance of the app.

Running that single command will show us the following output:

```shell
Preparing Electron app...

Converting icons...
Packaging... This will take a few seconds, maybe minutes if the requested Electron isn't cached yet...
Packaging app for platform linux x64 using electron v13.4.0
Finalizing build...

App built to /home/my-linux-user/NativeApps/Notion-linux-x64, move to wherever it makes sense for you and run the contained executable file (prefixing with ./ if necessary)

Menu/desktop shortcuts are up to you, because Nativefier cannot know where you're going to move the app. Search for "linux .desktop file" for help, or see https://wiki.archlinux.org/index.php/Desktop_entries
```

As shown in the output, the files will be in `/home/my-linux-user/NativeApps/Notion-linux-x64`. `cd` into this folder and you'll see a file named `Notion`. This is the main executable to launch our app. We'll need to give it the appropriate permissions.

```shell
cd Notion-linux-x64
chmod +x Notion
```

Now, execute `./Notion` and your Linux app should launch!

{% include article-image.html src="2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-notion.png" width="1024" height="768" alt="Notion on Ubuntu 20.04 as a native Linux app." %}

## Creating the app for Mastodon

Just like Notion, we'll create the app for [mastodon.technology](https://mastodon.technology).

{% include article-image.html src="2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-mastodon.png" width="1024" height="768" alt="Mastodon.technology on Ubuntu 20.04 as a Linux app." %}

Use the command:

```shell
nativefier -n Mastodon -p linux -a x64 --width 1024 --height 768 --tray --disable-dev-tools --single-instance https://mastodon.technology

cd Mastodon-linux-x64
chmod +x Mastodon
```

## Creating app for ayushsharma.in

For funzies, I'm going to create an app for this website as well. What good is having a tech blog if there's no Linux app for it :)

{% include article-image.html src="2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-ayushsharma-in.png" width="1024" height="768" alt="ayushsharma.in on Ubuntu 20.04 as a native Linux app." %}

Use the command:

```shell
nativefier -n ayushsharma -p linux -a x64 --width 1024 --height 768 --tray --disable-dev-tools --single-instance https://ayushsharma.in

cd ayushsharma-linux-x64
chmod +x ayushsharma
```

## Creating app for findmymastodon.com

And finally, I'll create an app for my pet project, [findmymastodon.com](https://findmymastodon.com).

{% include article-image.html src="2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-findmymastodon-com.png" width="1024" height="768" alt="findmymastodon.com on Ubuntu 20.04 as a native Linux app." %}

Use the command:

```shell
nativefier -n findmymastodon -p linux -a x64 --width 1024 --height 768 --tray --disable-dev-tools --single-instance https://findmymastodon.com

cd findmymastodon-linux-x64
chmod +x findmymastodon
```

## Creating Linux desktop icons

With our apps created and the executables ready-to-go, we can create desktop icons as well.

First, download the icons for [Notion](https://icons8.com/icons/set/Notion), [Mastodon](https://icons8.com/icons/set/mastodon), [ayushsharma.in](https://ayushsharma.in/static/images/meta/favicon/apple-touch-icon.png), and [findmymastodon.com](https://findmymastodon.com/static/images/favicon/apple-touch-icon.png). Place each icon in its Nativefier app directory as `icon.png`. Then, create the following `.desktop` files.

`Notion.desktop`

```ini
[Desktop Entry]
Type=Application
Name=Notion
Path=/home/my-linux-user/NativeApps/Notion-linux-x64
Exec=/home/my-linux-user/NativeApps/Notion-linux-x64/Notion
Icon=/home/my-linux-user/NativeApps/Notion-linux-x64/icon.png
```

`Mastodon.desktop`

```ini
[Desktop Entry]
Type=Application
Name=Mastodon
Path=/home/my-linux-user/NativeApps/Mastodon-linux-x64
Exec=/home/my-linux-user/NativeApps/Mastodon-linux-x64/Mastodon
Icon=/home/my-linux-user/NativeApps/Mastodon-linux-x64/icon.png
```

`ayushsharma.desktop`

```ini
[Desktop Entry]
Type=Application
Name=ayushsharma
Path=/home/my-linux-user/NativeApps/ayushsharma-linux-x64
Exec=/home/my-linux-user/NativeApps/ayushsharma-linux-x64/ayushsharma
Icon=/home/my-linux-user/NativeApps/ayushsharma-linux-x64/icon.png
```

`findmymastodon.desktop`

```ini
[Desktop Entry]
Type=Application
Name=findmymastodon
Path=/home/my-linux-user/NativeApps/findmymastodon-linux-x64
Exec=/home/my-linux-user/NativeApps/findmymastodon-linux-x64/findmymastodon
Icon=/home/my-linux-user/NativeApps/findmymastodon-linux-x64/icon.png
```

You can now move the `.desktop` files to your Linux desktop and see your linux apps all lined up :)

{% include article-image.html src="2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier-desktop-icons.png" width="1024" height="768" alt="Linux app icons for Notion, Mastodon, ayushsharma.in, and findmymastodon.com." %}

## Conclusion

Well wasn't that fun :) Nativefier might be the tool we've all been looking for. I know I have. My favourite feature is that once I log in to Notion or Mastodon I don't have to log in again! Nativefier runs Chromium underneath. So it's able to remember your session and works like it would in a regular browser. I have no idea if this is true or not. But at least that's how I think it works, which is enough for now :)

Special thanks to the Nativefier team for taking my Ubuntu one step closer to perfection :)