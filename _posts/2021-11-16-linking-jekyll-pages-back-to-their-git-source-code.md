---
layout: post
title:  "Linking Jekyll pages back to their Git source code"
number: 116
date:   2021-11-16 01:00
excerpt: "Add a link back to the public Git hosting link for any static Jekyll blog."
Category: Technology
banner_img: "2021-11-16-linking-jekyll-pages-back-to-their-git-source-code.jpg"
is_series: true
series_title: "Jekyll"
---
I'm a huge fan of open-source! One of the little ways I've supported the cause is by keeping this blog open since the very beginning. You can always [browse the full source-code here]({{ site.content.blog_source_code }}). The reason is partly to let people see the history of changes behind each page. But also because I didn't find many open-source Jekyll blogs to learn from when I started down this road. So I keep this website open in the hopes that my trials and errors will save someone else a lot of time.

I was recently asked by someone (shout-out to Seth!) to send them the Markdown source of one of my articles. This gave me the idea to add a permanent "View source" button to the footer of every article page. I'll walk you through the process of adding this to your own Jekyll website.

[Jekyll's helpful variables](https://jekyllrb.com/docs/variables/#page-variables) have exactly what I was looking for: the `page.path`. This variable contains the raw filesystem path of each page. The official description even highlights its usefulness for linking back to the source!

I'll use my article on creating [Linux apps for webapps]({% post_url 2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier%}) as an example. Printing `{%raw%}{{ page.path }}{%endraw%}` within the article's Markdown file outputs the following:

```shell
_posts/2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier.md
```

The above article's [source code](https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/master/_posts/2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier.md) is at the following path:

```shell
https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/master/_posts/2021-10-10-make-linux-apps-for-notion-mastodon-webapps-using-nativefier.md
```

This means that prepending `https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/master/` to any article's `page.path` will generate a link back to its source code!

In Jekyll, generating this full link will look like this:

```html
<a href="{%raw%}{{ page.path | prepend: site.content.blog_source_prefix }}{%endraw%}" target="_blank">View source</a>
```

And it's that's easy :)

## Conclusion
If you scroll down to the footer of this page, you should see a new "View source" link. Go ahead and try it out!