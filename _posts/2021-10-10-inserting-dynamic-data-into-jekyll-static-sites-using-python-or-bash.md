---
layout: post
title:  "Inserting dynamic data into Jekyll static sites using Python or Bash"
number: 111
date:   2021-10-10 02:00
excerpt: "Auto-generate Jekyll configuration files with content and avoid creating an API backend."
Category: Technology
is_series: true
series_title: "Jekyll"
---
[Jekyll]({% post_url 2016-08-15-introduction-to-jekyll %}), the static site generator, uses the `_config.yml` for configuration. The configurations are all Jekyll specific. But we can also [define variables with our own content]({% post_url 2021-08-16-using-variables-in-jekyll-to-define-custom-content %}) in these files and use them throughout our website. In this article, I'll highlight some advantages of dynamically creating Jekyll config files.

On my local laptop, I use the following command to serve my Jekyll website for testing:

```shell
bundle exec jekyll serve --incremental --config _config.yml
```

## Combining many configuration files

During local testing, it's sometimes necessary to override configuration options. My website's [current `_config.yml`](https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/2.0/_config.yml) has the following settings:

```yaml
# Jekyll Configuration

# Site Settings
url: "https://notes.ayushsharma.in"
website_url: "https://notes.ayushsharma.in/"
title: ayush sharma's notes ☕ + 🎧 + 🕹️
email: ayush@example.com
images-path: /static/images/
videos-path: /static/videos/
js-path: /static/js/
baseurl: "" # the subpath of your site, e.g. /blog
```

Since the local `jekyll serve` URL is `http://localhost:4000`, the URL defined above won't work. I can always create a copy of `_config.yml` as `_config-local.yml` and replace all the values. But there is an easier option.

Jekyll allows [specifying many configuration files](https://jekyllrb.com/docs/configuration/options/#build-command-options) with later declarations overriding previous ones. This means I can define a new `_config-local.yml` with the following code:

```yaml
url: ""
```

Then I can combine the above file with my main `_config.yml` like this:

```shell
bundle exec jekyll serve --incremental --config _config.yml,_config-local.yml
```

By combining both files, the final value of `url` for this Jekyll `serve` will be blank. This will turn all absolute URLs defined in my website into relative URLs and make them work on my local laptop.

## Combining dynamic config files
As a simple example, let's say you want to display the current date on your website. The bash command for this will be:

```shell
> date '+%A, %d %B %Y'
Saturday, 16 October 2021
```

We know we can [use Jekyll's `_config.yml`'s for custom content]({% post_url 2021-08-16-using-variables-in-jekyll-to-define-custom-content %}) as well. Let's output the above date into a new Jekyll config file.

```shell
my_date=`date '+%A, %d %B %Y'`; echo 'my_date: "'$my_date'"' > _config-data.yml
```

Now `_config-data.yml` contains:

```yaml
my_date: "Saturday, 16 October 2021"
```

We can combine our new config file with the others and use the `my_date` variable in our website.

```shell
bundle exec jekyll serve --incremental --config _config.yml,_config-local.yml,_config-data.yml
```

On running the above command, `{% raw %}{{ site.my_date }}{% endraw %}` will output its configured value.

## Conclusion

The example above is quite simple but the possibilities are endless. Bash, Python, and other programming languages can dynamically generate Jekyll config files. We can then combine these during the `build` or `serve` process.

For [findmymastodon.com](https://findmymastodon.com), [I'm using Python to fetch Mastodon user statistics](https://gitlab.com/ayush-sharma/find-my-mastodon/-/blob/1.0/src/fetch-instance-data/fetch_data.py#L252). I'm then writing these into a new [`_config-data.yml` file](https://gitlab.com/ayush-sharma/find-my-mastodon/-/blob/1.0/_config-data.yml) (currently manually). Finally, the [home page and others display these from the configuration file](https://gitlab.com/ayush-sharma/find-my-mastodon/-/blob/1.0/index.md#L16). This way I can leverage a dynamic backend and still keep all the static website goodness I'm so fond of.

I hope this has sparked some ideas for your own static websites. The JAMStack is great for static websites. But we can avoid creating an entire API backend for dynamic content. Why not instead use a build job to create config files with updated content? It might not suit every use-case, but one less API means fewer infrastructure moving parts.

I hope this helps you in some way during your next static website project. Keep reading, and happy coding :)