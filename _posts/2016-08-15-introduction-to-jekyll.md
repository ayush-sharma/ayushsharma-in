---
layout: post
title:  "Introduction to Jekyll"
number: 12
date:   2021-08-21 02:00
original_date:   2016-08-15 02:00
excerpt: "Jekyll is a framework for building websites - write your content in Markdown, use HTML/CSS for structure and presentation, and Jekyll compiles it all into static HTML. No servers. No backends. No fuss."
Category: Technology
banner_img: "2016-08-15-introduction-to-jekyll-new-article.png"
is_series: true
series_title: "Jekyll"
---
Static website generators and JAMStack have taken off in recent years. And with good reason. With only static HTML, CSS, and Javascript to serve, there is no need for complex backends. Not having backends means better security, lower operational overhead, and cheaper hosting. A win-win-win!

In this article I'm going to talk about Jekyll. As of this writing, [this website uses Jekyll]({{ site.content.blog_source_code }}). Jekyll uses a Ruby engine to convert articles written in Markdown to generate HTML. Sass allows merging complex CSS rules into flat files. Liquid allows some programmatic control over otherwise static content.
 
## Installation
The [Jekyll website has installation instructions for Linux, MacOS, and Windows](https://jekyllrb.com/docs/installation/). After installation, the [Quickstart guide](https://jekyllrb.com/docs/) will set up a basic Hello-World project.

Now visit `http://localhost:4000` in your browser. You should see your default "awesome" blog.

{% include article-image.html src="2016-08-15-introduction-to-jekyll-welcome-to-jekyll.png" width="900" alt="Hello World example with Jekyll." %}

## Directory Structure
The default site contains the following files and folders:

- `_posts`: Your blog entries.
- `_site`: The final compiled static website.
- `about.markdown`: Content for the about page.
- `index.markdown`: Content for the home page.
- `404.html`: Content for the 404 page.
- `_config.yml`: Site-wide configuration for Jekyll. 

## Creating new blog entries
Creating posts is simple. All you need to do is create a new file under `_posts` with the proper format and extension, and you're all set.

A valid file name is `2021-08-29-welcome-to-jekyll.markdown`. A post file must contain what Jekyll calls the YAML Front Matter. It's a special section at the beginning of the file with metadata. If you see the default post, you'll see the following:

```yaml
---
layout: post
title:  "Welcome to Jekyll!"
date:   2021-08-29 11:28:12 +0530
categories: jekyll update
---
```

Jekyll uses the above metadata and you can also define custom `key: value` pairs. If you need some inspiration, [have a look at my website's front matter](https://gitlab.com/ayush-sharma/ayushsharma-in/-/blob/2.0/_posts/2021-07-15-the-evolution-of-ayushsharma-in.md). Aside from the front matter, you can [use in-built Jekyll variables](https://jekyllrb.com/docs/variables/) to customize your website.

Let's create a new post. Create `2021-08-29-ayushsharma.markdown` in the `_posts` folder. Add the following content:

```yaml
---
layout: post
title:  "Check out ayushsharma.in!"
date:   2021-08-29 12:00:00 +0530
categories: mycategory
---
This is my first post.

# This is a heading.

## This is another heading.

This is a [link](http://notes.ayushsharma.in)

This is my category: {{ page.categories }}
```

If the `jekyll serve` command is still running, refresh the page and you'll see the new entry below.

{% include article-image.html src="2016-08-15-introduction-to-jekyll-new-article.png" width="900" alt="New article created in Jekyll." %}

Congrats on creating your first article! The process may seem simple, but there's a lot you can do with Jekyll. Using simple markdown, you can generate an archive of posts, syntax highlighting for code snippets, and separate pages for posts in one category.

## Drafts
If you're not ready to publish your content yet, you can create a new `_drafts` folder. Markdown files in this folder are only rendered by passing the `--drafts` argument.

## Layouts And Includes
Note the front matter of the two articles in our `_posts` folder, and you'll see `layout: post` in the Front Matter. The `_layout` folder contains all the layouts. You won't find them in your source code because Jekyll loads them by default. The default source code used by Jekyll is [here](https://github.com/jekyll/minima/blob/master/_layouts/post.html). If you follow the link, you'll see that the `post` layout uses the [`default` layout](https://github.com/jekyll/minima/blob/master/_layouts/default.html#L12). The default layout contains the code `{% raw %}{{ content }}{% endraw %}` which is where content is injected. The layout files will also contain `include` directives. These load files from the [`includes` folder](https://github.com/jekyll/minima/tree/master/_includes) and allow composing a page using different components.

Overall, this is how layouts work - you define them in the front matter, and inject your content within them. Includes provide other sections of the page to compose a whole page. This is a standard web-design technique - defining header, footer, aside, and content elements and then injecting content within them. This is real power of static site generators - full programmatic control over assembling your website with final compilation into static HTML.

## Pages
Not all content on your website will be an article or a blog post. You'll need about pages, contact pages, project pages, or portfolio pages. This is where Pages come in. They work exactly like Posts do, meaning they're markdown files with front matter. But they don't go in the `_posts` directory. They either stay in your project root or in folders of their own. For Layouts and Includes, you can use the same ones as you do for your Posts or create new ones. Jekyll is very flexible and you can be as creative as you want! Your default blog already has `index.markdown` and `about.markdown`. Feel free to customize them as you wish.

## Data Files
Data files live in the `_data` directory, and can be `.yml`, `.json`, or `.csv`. For example, a `_data/members.yml` file may contain:

```yaml
- name: A
 github: a@a

- name: B
 github: b@b

- name: C
 github: c@c
```

Jekyll reads these during site generation. You can access them using `site.data.members`.

```html
<ul>
{ % for member in site.data.members %}
 <li>
 <a href="https://github.com/{{ member.github }}">
      { { member.name }}
 </a>
 </li>
{ % endfor %}
</ul>
```
## Permalinks
Your `_config.yml` file defines the format of your permalinks. You can [use a variety of default variables](https://jekyllrb.com/docs/permalinks/) to assemble your own custom permalink.

## Building your final website
The command `jekyll serve` is great for local testing. But once you're done with local testing, you'll want to build the final artifact to publish. The command `jekyll build --source source_dir --destination destination_dir` builds your website into the `_site` folder. Note that this folder is clean up before every build, so don't place important things in there. Once you have the content, you can host it on a static hosting service of your choosing.

You should now have a decent overall grasp of what Jekyll is capable of and what the main bits and pieces do. If you're looking for inspiration, the official [JAMStack website has some amazing examples](https://jamstack.org/examples/).

{% include article-image.html src="2016-08-15-introduction-to-jekyll-jamstack-examples.png" width="900" alt="New article created in Jekyll." %}

Happy coding :)