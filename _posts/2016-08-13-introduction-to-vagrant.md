---
layout: post
title:  "Introduction to Vagrant"
number: 6
date:   2021-08-21 01:00
original_date:   2016-08-13 01:00
excerpt: "Vagrant helps you run other operating systems on your computer, meaning you can build things, test things, and do crazy shit without blowing up your own system."
Category: Technology
is_series: true
series_title: "Vagrant"
---
I've been happy using Vagrant for quite a while now. I work with AWS and DevOps tools, and installing them all on one system can mess things up. Vagrant is great for doing crazy things without actually breaking your system. And if you're not already using it, you should start now.

If you're familiar with VirtualBox or VMware then learning Vagrant will be easy. Think of Vagrant as simple interface in front of VirtualBox or VMware. A single config file, called `Vagrantfile`, allows you to customize your virtual machines, also called Vagrant boxes. And a simple CLI interface lets you start, stop, suspend, or destroy your boxes.

Consider this simple example.

Let's say you want to write Ansible or shell scripts to install Nginx on a new server. You can't do it on your own system because you might not have the right operating system or dependencies. And launching new cloud servers for testing might be time-consuming and expensive. This is where Vagrant comes in. You can use it to bring up a local Ubuntu box and provision it using your scripts. Then delete the box, re-provision it, and re-run your scripts to verify your changes. You can repeat this process as many times as you want until you're confident your scripts work. And you can commit your Vagrantfile in Git to ensure your team is testing on the same environment. No more "...but it works fine on my machine"!

## Getting Started
First, [install Vagrant on your system](https://www.vagrantup.com/docs/installation) and then create a new folder to experiment in. In this new folder, create a new file named `Vagrantfile` with these contents:

```ruby
Vagrant.configure("2") do |config|
  
  config.vm.box = "ubuntu/hirsute64"
  
end
```

You can also run `vagrant init ubuntu/hirsute64` and it will generate a new Vagrantfile for you. Now run `vagrant up`. This command will download the `ubuntu/hirsuite64` image from the Vagrant registry.

```shell
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'ubuntu/hirsute64'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'ubuntu/hirsute64' version '20210820.0.0' is up to date...
==> default: Setting the name of the VM: a_default_1630204214778_76885
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: Warning: Remote connection disconnect. Retrying...
    default: Warning: Connection reset. Retrying...
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
```

At this point, if you open your Vagrant backend (either VirtualBox or VMware) you'll see your box there. Next, run `vagrant ssh` to log in to the box. If you can see the Vagrant prompt, then you're in!

```shell
~ vagrant ssh
Welcome to Ubuntu 21.04 (GNU/Linux 5.11.0-31-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun Aug 29 02:33:51 UTC 2021

  System load:  0.01              Processes:               110
  Usage of /:   4.1% of 38.71GB   Users logged in:         0
  Memory usage: 17%               IPv4 address for enp0s3: 10.0.2.15
  Swap usage:   0%                IPv4 address for enp0s8: 192.168.1.20


0 updates can be applied immediately.


vagrant@ubuntu-hirsute:~$
```

Vagrant uses "base boxes" to bring up your local machines. In our case, Vagrant downloads the `ubuntu/hirsuite64` image from [Hashicorp's Vagrant catalogue](https://app.vagrantup.com/boxes/search) and plugs in to VirtualBox or VMware to create the actual box.

## Shared Folders
Vagrant maps your current folder as `/vagrant` within the Vagrant box. This allows you to keep your files in sync on your system and within the box. This is great for testing a Nginx website by pointing your document root to `/vagrant`. You can use an IDE to make changes and Nginx within the box will serve them.

## Vagrant Commands
There are several Vagrant commands which you can use to control your box.

Some of the important ones are:

- `vagrant up`: Bring a box online.
- `vagrant status`: Show current box status.
- `vagrant suspend`: Pause the current box.
- `vagrant resume`: Resume the current box.
- `vagrant halt`: Shutdown the current box.
- `vagrant destroy`: Destroy the current box. By running this command, you will lose any data stored on the box.
- `vagrant snapshot`: Take a snapshot of the current box.
 
## Resources
- [https://www.vagrantup.com/](https://www.vagrantup.com/)
- [http://www.vagrantbox.es/](http://www.vagrantbox.es/)
- [https://github.com/mitchellh/vagrant-aws](https://github.com/mitchellh/vagrant-aws)