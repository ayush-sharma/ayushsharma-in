---
title: About Me
desc: "(づ｡◕‿‿◕｡)づ"
layout: page
permalink: /about
---
<div class="row">
	<div class="col-10 offset-1">
		<h2 class="bullet-pointer">Projects</h2>
		<p>I'm usually building <a href="https://gitlab.com/ayush-sharma/ayushsharma-in" title="My little corner of the internet. A digital garden with writings on technology, video games, and life stuff." target="_blank">ayush.nz</a>, <a href="https://www.bbkidsrehab.com" title="Pro-bono webdev for Building Blocks, a rehab clinic in New Delhi, India, which specialises in Physiotherapy and Counselling Services." target="_blank">bbkidsrehab.com</a>, and <a href="https://www.fediverse.to" title="Find a decentralised social network which suits your interests. Search and filter the fediverse by category, language, and more." target="_blank">to-the-fediverse.to</a> during my free time.</p>
		<h2 class="bullet-pointer">Featured</h2>
		<p>I have previously written for <a href="https://dzone.com/users/3717581/ayushsharma23.html" target="_blank">DZone</a>, <a href="https://bitbucket.org/blog/author/a-sharma" target="_blank">Bitbucket</a>, and <a href="https://opensource.com/users/ayushsharma" target="_blank">OpenSource.org</a>.</p>
		<h2 class="bullet-pointer">Get in touch</h2>
		{% include bar-social-nav.html %}
	</div>
</div>