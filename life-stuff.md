---
title: Life stuff
layout: page
desc: "(҂◡_◡) ᕤ"
permalink: /life-stuff
---
{% assign posts = site.posts| where:"Category", "Life Stuff" | sort: 'date' | reverse %}
{% for post in posts %}
{% include article-link.html post=post %}
{% endfor %}